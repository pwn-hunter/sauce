TERMUX_PKG_HOMEPAGE=https://github.com/alsa-project/alsa-lib
TERMUX_PKG_DESCRIPTION="The Advanced Linux Sound Architecture (ALSA) - library"
TERMUX_PKG_LICENSE="BSD" # Leaved like that as none is specified
TERMUX_PKG_MAINTAINER="@Hilledkinged"
TERMUX_PKG_VERSION=1.2.4
TERMUX_PKG_REVISION=2
TERMUX_PKG_SRCURL=https://github.com/alsa-project/alsa-lib/archive/refs/tags/v1.2.4.tar.gz
TERMUX_PKG_SHA256=0c6ab052d7ea980a01d0208da5e5e10849bd16c4c9961bbd5d2665083b74a6c0
TERMUX_PKG_DEPENDS="pulseaudio, libx11"
TERMUX_PKG_EXTRA_CONFIGURE_ARGS="--disable-static --disable-mixer --disable-hwdep --disable-rawmidi
--disable-seq --disable-instr --disable-alisp
--with-pcm-plugins=no --with-libdl=no"
TERMUX_PKG_BUILD_IN_SRC=true

termux_step_pre_configure() {
	touch ltconfig
	libtoolize --force --copy --automake
	aclocal
	autoheader
	automake --foreign --copy --add-missing
	touch depcomp
	autoconf -f
}
