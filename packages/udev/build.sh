TERMUX_PKG_HOMEPAGE=https://github.com/FreeBSDDesktop/libudev-devd
TERMUX_PKG_DESCRIPTION="udev - Linux userspace device management"
TERMUX_PKG_LICENSE="BSD" # Leaved like that as none is specified
TERMUX_PKG_MAINTAINER="@termux"
TERMUX_PKG_VERSION=182
TERMUX_PKG_REVISION=1
TERMUX_PKG_SRCURL=https://git.kernel.org/pub/scm/linux/hotplug/udev.git/snapshot/udev-$TERMUX_PKG_VERSION.tar.gz
TERMUX_PKG_SHA256=ab462c046189db86edb325c5c1976e17a33a0d6bef40a6dbbec8c07ceb2a40ff
TERMUX_PKG_DEPENDS="libkmod"
TERMUX_PKG_BUILD_IN_SRC=true

termux_step_pre_configure() {
	autoreconf --install --symlink
}
