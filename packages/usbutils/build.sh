TERMUX_PKG_HOMEPAGE=http://www.linux-usb.org/
TERMUX_PKG_DESCRIPTION="USB utilities for Linux, including lsusb"
TERMUX_PKG_LICENSE="GPL2"
TERMUX_PKG_MAINTAINER="@Hilledkinged"
TERMUX_PKG_VERSION=v013
TERMUX_PKG_REVISION=1
TERMUX_PKG_SRCURL=https://github.com/gregkh/usbutils/archive/refs/heads/master.tar.gz
TERMUX_PKG_SHA256=51bf58c27c4d654cc0df58218aec208609770920bfbda1f3b9e4ce9671d20b5e
TERMUX_PKG_DEPENDS="apt, bash, libusb"
TERMUX_PKG_EXTRA_CONFIGURE_ARGS="--disable-static"
TERMUX_PKG_BUILD_IN_SRC=true

termux_step_pre_configure() {
	cd usbhid-dump
	autoreconf -i -f
	cd ..

	autoreconf --install --symlink

	find configure.ac -type f -exec sed -i 's+PKG_CHECK_MODULES(UDEV, libudev >= 196)+# nope+g' {} +
}
