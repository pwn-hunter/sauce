TERMUX_PKG_HOMEPAGE=http://bluez.org
TERMUX_PKG_DESCRIPTION="Digital audio encoder and decoder used to transfer data to Bluetooth audio output devices"
TERMUX_PKG_LICENSE="GPL-2.0"
TERMUX_PKG_VERSION=1.4
TERMUX_PKG_SRCURL=https://www.kernel.org/pub/linux/bluetooth/sbc-${TERMUX_PKG_VERSION}.tar.xz
TERMUX_PKG_SHA256=518bf46e6bb3dc808a95e1eabad26fdebe8a099c1e781c27ed7fca6c2f4a54c9
