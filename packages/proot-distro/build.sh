TERMUX_PKG_HOMEPAGE=https://github.com/termux/proot-distro
TERMUX_PKG_DESCRIPTION="Termux official utility for managing proot'ed Linux distributions"
TERMUX_PKG_LICENSE="GPL-3.0"
TERMUX_PKG_MAINTAINER="Leonid Pliushch <leonid.pliushch@gmail.com>"
TERMUX_PKG_VERSION=1.6.1
TERMUX_PKG_SRCURL=https://gitlab.com/pwn-hunter/packages/proot-distro/-/archive/${TERMUX_PKG_VERSION}/proot-distro-v${TERMUX_PKG_VERSION}.tar.gz
TERMUX_PKG_SHA256=d68b10c3258357444df30ba3d028a5877f52ec9fe882aa1e0ccaca7a8e5eff72
TERMUX_PKG_DEPENDS="bash, bzip2, coreutils, curl, findutils, gzip, ncurses-utils, proot, sed, tar, xz-utils"
TERMUX_PKG_BUILD_IN_SRC=true
TERMUX_PKG_PLATFORM_INDEPENDENT=true

# Allow to edit distribution plug-ins.
TERMUX_PKG_CONFFILES="
etc/proot-distro/alpine.sh
etc/proot-distro/archlinux.sh
etc/proot-distro/debian-buster.sh
etc/proot-distro/fedora-33.sh
etc/proot-distro/nethunter.sh
etc/proot-distro/parrot-lts.sh
etc/proot-distro/ubuntu-18.04.sh
etc/proot-distro/ubuntu-20.04.sh
"

termux_step_make_install() {
	./install.sh
}
