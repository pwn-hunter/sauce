TERMUX_PKG_HOMEPAGE=https://git.kernel.org/pub/scm/utils/kernel/kmod/kmod.git/
TERMUX_PKG_DESCRIPTION="Linux kernel module handling"
TERMUX_PKG_LICENSE="BSD" # Leaved like that as none is specified
TERMUX_PKG_MAINTAINER="@Hilledkinged"
TERMUX_PKG_VERSION=28
TERMUX_PKG_REVISION=4
TERMUX_PKG_SRCURL=https://gitlab.com/pwn-hunter/packages/kmod/-/archive/master/kmod-master.tar.gz
TERMUX_PKG_SHA256=46ee77432a4b51185dad9484b0c9463bb86d3f0974f5f2b3614f9267a7fa9e9b
TERMUX_PKG_DEPENDS="apt, bash"
TERMUX_PKG_EXTRA_CONFIGURE_ARGS="--disable-static"
TERMUX_PKG_BUILD_IN_SRC=true

termux_step_pre_configure() {
	./autogen.sh
}

termux_step_make_install() {
	cd tools
	cp -rf kmod $TERMUX_PREFIX/bin
}
