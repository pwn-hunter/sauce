#!/bin/bash
echo "Cleaning old debs (In case of bad exit)"
rm -rf *.deb

echo "Building new packages"
dpkg-deb --build apktool
dpkg-deb --build metasploit-framework
dpkg-deb --build neofetch
dpkg-deb --build openjdk-jdk8
dpkg-deb --build openjdk-jdk11
dpkg-deb --build cus-glib
dpkg-deb --build ngrok
dpkg-deb --build msfpc
dpkg-deb --build nethunter-support
dpkg-deb --build android-pin-bruteforce
dpkg-deb --build john
dpkg-deb --build sysroot-extra
dpkg-deb --build psf

mv -f *.deb ../debs
echo "Done"

